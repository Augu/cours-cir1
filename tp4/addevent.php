<?php
/**
 * @var string $day     Event day
 */
?>

<div class="container text-center mt-5">
    <div id="backToHome">
        <a href="./"><i class="fa fa-chevron-circle-left fa-3x" aria-hidden="true"></i></a>
    </div>

    <h1>Ajouter un événement</h1>
    <form class="mt-5" method="post" action="./?add&submitted">
        <input type="text" id="name" name="name" class="form-control" placeholder="Nom de l'événement" required>
        <br>
        <textarea id="description" name="description" class="form-control" placeholder="Description de l'événement" required></textarea>
        <br>
        <div class="row">
            <div class="col">
                <label for="nb_place">Nombre de places</label>
            </div>
            <div class="col-8">
                <input type="number" id="nb_place" name="nb_place" class="form-control" value="10" required>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col">
                <label for="startdate">Début de l'événement</label>
            </div>
            <div class="col-8">
                <input type="datetime-local" id="startdate" name="startdate" class="form-control" value="<?= htmlspecialchars($day, ENT_QUOTES); ?>T08:00" required>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col">
                <label for="enddate">Fin de l'événement</label>
            </div>
            <div class="col-8">
                <input type="datetime-local" id="enddate" name="enddate" class="form-control" value="<?= htmlspecialchars($day, ENT_QUOTES); ?>T20:00" required>
            </div>
        </div>
        <br>
        <input type="submit" class="btn btn-lg btn-primary btn-block" value="Créer l'événement">
    </form>
</div>
